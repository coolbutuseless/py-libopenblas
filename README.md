Python CFFI interface to openblas.
=================================
This is a small bit of code to get [openblas](www.openblas.net) accessible from python using [cffi](https://bitbucket.org/cffi/cffi).  It assumes you have already downloaded `openblas` for your system and have it installed somewhere in your library search path.

Example - Dot product of 2 vectors
==================================
* See `example_dot.py` for a demonstration of:
    - using cffi to allocation some memory 
    - calling libopenblas to calculate the dotproduct

OSX OpenBLAS installation
=====================
* port install openblas
* `export DYLD_FALLBACK_LIBRARY_PATH="/opt/local/lib"`


References
==========
* Helpful notes from someone else going through creation of a CFFI wrapper for a library: [Python interface to signalfd using FFI](http://blog.oddbit.com/2013/11/28/a-python-interface-to-signalfd/)
