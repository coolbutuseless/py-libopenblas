#!/usr/bin/env python
from __future__ import print_function, division

from cffi import FFI
import numpy as np
ffi = FFI()

ffi.cdef(open("pyopenblas.h").read())
openblas = ffi.dlopen("/opt/local/lib/libopenblas-r0.2.13.dylib")

N = 4
A = np.random.rand(N*N).reshape((N, N))
a = np.random.rand(N)
x = np.random.rand(N)
y = np.random.rand(N)


pA = ffi.cast("double *", A.__array_interface__['data'][0]) 
px = ffi.cast("double *", x.__array_interface__['data'][0]) 
py = ffi.cast("double *", y.__array_interface__['data'][0]) 
pa = ffi.cast("double *", y.__array_interface__['data'][0]) 


if False:
    A.dot(x, out=y)
    print("numpy:", y)

    y[0] = 999

    order = openblas.CblasRowMajor
    transpose = openblas.CblasNoTrans
    alpha = 1.0
    beta = 0.0
    incx = incy = 1

    openblas.cblas_dgemv(order, transpose, N, N, alpha, pA, N, px, incx, beta, py, incy)
    print("oblas:", y)

if False:
    print("numpy:", np.linalg.norm(x))

    #double cblas_dnrm2 (const blasint N, const double *X, const blasint incX);
    res = openblas.cblas_dnrm2(N, px, 1)
    print("oblas:", res)

if False:
    x0 = x.copy()
    x0 *= 1.2
    print("numpy:", x0)

    openblas.cblas_dscal(N, 1.2, px, 1)
    print("oblas:", x)

if False:
    print("numpy:", x.argmax())
    res = openblas.cblas_idamax(N, px, 1)
    print("oblas:", res)

# void cblas_dcopy(const blasint n, const double *x, const blasint incx, double *y, const blasint incy);
x[0] = 42.0
print("pre:", x, y)
openblas.cblas_dcopy(N, px, 0, py, 1)
print("pre:", x, y)

