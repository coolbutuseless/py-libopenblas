#!/usr/bin/env python
import os
import re

# Header could just be generated with:
#    gcc -E openblas_headers/cblas_openblas.h > libopenblas.h
#
# But that results in a lot of cruft, so doing it manually for now.


function_count = 0

with open('build_headers/libopenblas_bottom.h', 'w') as out:
    out.write("/* Declarations below has been automatically extracted from 'cblas_openblash.h' */\n\n")
    with open('openblas_headers/cblas_openblas.h') as header:
        text = header.read()
        functions = re.findall("(?:int|void|float|double|openblas_complex|char|CBLAS_INDEX).*\(.*?\);", text)
        for function in functions:
            function = function.strip()
            function = function.replace("OPENBLAS_CONST", "const")
            function = function.replace("CBLAS_INDEX", "size_t")
            if "openblas_complex" in function:
                function = "/* Complex types not supported by CFFI 0.8.6 */\n/* " + function + "*/"
            function_count += 1
            out.write(function + "\n")

os.system("cat build_headers/libopenblas_top.h build_headers/libopenblas_bottom.h > libopenblas.h")

print("Parsed %i functions from cblas_openblas.h" % function_count)
