#!/usr/bin/env python
from __future__ import print_function

from libopenblas import libopenblas

from cffi import FFI
ffi = FFI()

# Create 2 arrays of length 4
a = ffi.new("double[]", 4)
b = ffi.new("double[]", 4)

for i in range(4):
    a[i] = i
    b[i] = i+1

print("Array a: ", [a[i] for i in range(4)])
print("Array b: ", [b[i] for i in range(4)])

# OpenBLAS declaration for dot product of two arrays of doubles
# double cblas_ddot(const blasint n, const double *x, const blasint incx, const double *y, const blasint incy);
result = libopenblas.cblas_ddot(4, a, 1, b, 1)
print("Dot product:", result)
print("Correct result?: ", result==sum(ai * bi for ai, bi in zip(a, b)))
