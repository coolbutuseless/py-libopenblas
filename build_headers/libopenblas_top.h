/* 
 * echo '#include <stddef.h>' | gcc -E - | grep typedef
 */
/* typedef long int ptrdiff_t;
typedef long unsigned int size_t;
typedef int wchar_t;
*/
/* 
 * cat openblas_headers/openblas_config.h | gcc -E - | grep typedef | less
 */

typedef long BLASLONG;
typedef unsigned long BLASULONG;
typedef BLASLONG blasint;

/* CFFI 0.8.6 doesn't support complex numbers out of the box.
 * Need to write wrapper functions for structs. */
/*
typedef float _Complex openblas_complex_float;
typedef double _Complex openblas_complex_double;
typedef long double _Complex openblas_complex_xdouble;
*/

typedef enum CBLAS_ORDER     {CblasRowMajor=101, CblasColMajor=102} CBLAS_ORDER;
typedef enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113, CblasConjNoTrans=114} CBLAS_TRANSPOSE;
typedef enum CBLAS_UPLO      {CblasUpper=121, CblasLower=122} CBLAS_UPLO;
typedef enum CBLAS_DIAG      {CblasNonUnit=131, CblasUnit=132} CBLAS_DIAG;
typedef enum CBLAS_SIDE      {CblasLeft=141, CblasRight=142} CBLAS_SIDE;

