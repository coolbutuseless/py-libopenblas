#!/usr/bin/env python
from __future__ import print_function, absolute_import, division
import os
from cffi import FFI
ffi = FFI()

this_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
with open(this_dir + "libopenblas.h") as header:
    ffi.cdef(header.read())

# Ensure libopenblas.dylib is somewhere in your LD search path
# Might need to set LDFLAGS/DYLD_FALLBACK_LIBRARY_PATH
libopenblas = ffi.dlopen("libopenblas.dylib")

# Num threads is set correctly on OSX.
# If you want to set it manaully, set it to # of *REAL* cores. hyperthreaded cores don't count
# libopenblas.openblas_set_num_threads(4)
